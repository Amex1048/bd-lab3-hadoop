#!/bin/sh
set -e

alias hdfs="hadoop/bin/hdfs dfs"

hdfs -mkdir -p /user/$(whoami)
hdfs -mkdir -p data
hdfs -mkdir -p map-reduce
hdfs -put data/airlines.csv data
hdfs -put data/flights.csv data
hdfs -put map-reduce/* map-reduce
