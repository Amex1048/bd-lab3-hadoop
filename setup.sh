#!/bin/sh
set -e

hadoop/bin/mapred streaming \
    -input data \
    -output out \
    -mapper map-reduce/map.py \
    -reducer map-reduce/reduce.py
clear
hadoop/bin/hdfs dfs -cat out/part-00000
