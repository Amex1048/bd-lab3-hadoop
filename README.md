## Lab3: Hadoop

### Setup project

1. Copy dataset `airlines.csv` and `flights.csv` to `./data` dir from [here](https://www.kaggle.com/datasets/usdot/flight-delays/download?datasetVersionNumber=1).

2. Download hadoop:
```
$ wget https://dlcdn.apache.org/hadoop/common/hadoop-3.3.6/hadoop-3.3.6.tar.gz
$ tar -xf hadoop-3.3.6.tar.gz
$ mv hadoop-3.3.6 hadoop
```
3. Setup hadoop in *Pseudo-Distributed Mode* using [official guide](https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html).

4. Format hadoop file system:
```
$ ./hadoop/bin/hdfs namenode -format
```
5. Start NameNode daemon and DataNode daemon:
```
$ ./hadoop/sbin/start-dfs.sh
```
6. Copy files to hdfs and run the job:
```
$ ./copy.sh
$ ./setup.sh
```

### Clear
To clear created artifacts:
```
$ ./clear.sh
$ ./hadoop/sbin/stop-dfs.sh
```
